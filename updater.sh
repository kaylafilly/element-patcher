#!/usr/bin/env sh

dir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
cd $dir
url="https://github.com/vector-im/element-web/releases.atom"
feed="$(curl --silent --fail "$url")"

last_release="$(touch last_release && cat ./last_release)"
latest_release="$(echo $feed | xmllint --xpath "//*[local-name()='entry'][1]/*[local-name()='title']/text()" -)"

if [ "$latest_release" != "$last_release" ] 
    then
        echo "New release found ($latest_release)"
    else
        echo "No new version found"
        exit
fi

if [ ! -d "./matrix-js-sdk" ]; then
    echo "matrix-js-sdk does not exist, grabbing it..."
    git clone https://github.com/matrix-org/matrix-js-sdk.git
    pushd matrix-js-sdk
    git fetch --tags
    tag=$(git describe --tags `git rev-list --tags --max-count=1`)
    git checkout $tag && yarn link && yarn install
    popd
else
    pushd matrix-js-sdk
    git fetch --tags
    tag=$(git describe --tags `git rev-list --tags --max-count=1`)
    git checkout $tag && yarn install
    popd
fi

if [ ! -d "./matrix-react-sdk" ]; then
    echo "matrix-react-sdk does not exist, grabbing it..."
    git clone https://github.com/matrix-org/matrix-react-sdk.git
    pushd matrix-react-sdk
    git fetch --tags
    tag=$(git describe --tags `git rev-list --tags --max-count=1`)
    git checkout $tag && yarn link && yarn link matrix-js-sdk && yarn install
    popd
else
    pushd matrix-react-sdk
    git fetch --tags
    tag=$(git describe --tags `git rev-list --tags --max-count=1`)
    git checkout $tag && yarn install
    popd
fi

if [ ! -d "./element-web" ]; then
    echo "element-web does not exist, grabbing it..."
    git clone https://github.com/vector-im/element-web.git
    pushd element-web
    git fetch --tags
    tag=$(git describe --tags `git rev-list --tags --max-count=1`)
    git checkout $tag

    #TODO: add greentext and embed patch here

    yarn link matrix-js-sdk  && yarn link matrix-react-sdk
    popd
else
    pushd element-web
    git fetch --tags
    tag=$(git describe --tags `git rev-list --tags --max-count=1`)
    git checkout $tag
    popd
fi

cd element-web
echo "Building Element..."
cp -f ../config.json ./config.json
cp -f ../lake.jpg ./res/themes/element/img/backgrounds/lake.jpg 
patch -uf scripts/package.sh -i ../package.patch -r -
version=`git describe --tags || echo unknown`
yarn install && yarn dist
mkdir ../element-dist
tar -xvf dist/element-$version.tar.gz -C ../element-dist
rm -rf dist
cd ..

cd element-dist/element-$version
cp -f ../../config.json ./config.json
echo "Pushing Element to wrangler..."
wrangler pages publish ./ --project-name element-web --branch production
cd ../..

rm -rf element-dist
echo "Done!"
echo $latest_release > ./last_release
